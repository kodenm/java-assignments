package ikramarev.java.fifthassignment;

import ikramarev.java.fifthassignment.annotations.AutoInjectable;
import org.junit.Assert;
import org.junit.Test;

import java.util.Properties;

public class InjectorTests {
    @Test
    public void Injector_NoFieldTypeNameFoundInPropertiesFile_ObjectMustBeNull() {
        // arrange
        var properties = new Properties();
        var injector = new Injector(properties);

        // act
        var injectedObject = injector.inject(new SomeBean());

        // assert
        Assert.assertEquals(injectedObject, null);
    }

    @Test
    public void Injector_UnableToCreateInstanceOfTypeInPropertiesFile_ObjectMustBeNull() {
        // arrange
        var properties = new Properties();
        properties.put("ikramarev.java.fifthassignment.interfaces.SomeInterface","ikramarev.java.fifthassignment.implementations.UnknownType");
        properties.put("ikramarev.java.fifthassignment.interfaces.SomeOtherInterface","ikramarev.java.fifthassignment.implementations.UnknownType");
        var injector = new Injector(properties);

        // act
        var injectedObject = injector.inject(new SomeBean());

        // assert
        Assert.assertEquals(injectedObject, null);
    }

    @Test
    public void Injector_AllFieldAndInjectedTypeNamesArePresent_InjectedObjectReturned() {
        // arrange
        var properties = new Properties();
        properties.put("ikramarev.java.fifthassignment.interfaces.SomeInterface","ikramarev.java.fifthassignment.implementations.OtherImpl");
        properties.put("ikramarev.java.fifthassignment.interfaces.SomeOtherInterface","ikramarev.java.fifthassignment.implementations.SODoer");
        var injector = new Injector(properties);

        // act
        var injectedObject = injector.inject(new SomeBean());
        var injectedFields = injectedObject.getClass().getDeclaredFields();

        // assert
        Assert.assertNotEquals(injectedObject, null);
        for (var field : injectedFields) {
            if (field.isAnnotationPresent(AutoInjectable.class)) {
                try {
                    var fieldTypeName = field.getType().getName();
                    field.setAccessible(true);
                    var fieldValueTypeName = field.get(injectedObject);
                    Assert.assertEquals(properties.get(fieldTypeName), fieldValueTypeName.getClass().getName());
                }
                catch (Exception e) {
                    Assert.fail();
                }
            }
        }
    }
}
