package ikramarev.java.fifthassignment;

import ikramarev.java.fifthassignment.annotations.AutoInjectable;

import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.Properties;

public class Injector {
    private Properties properties;

    public Injector() {
        try {
            properties = new Properties();
            properties.load(new FileReader(new File(this.getClass().getClassLoader().getResource("app.properties").toURI())));
        }
        catch (Exception e) {
            System.out.println("Properties file not found");
        }
    }

    public Injector(Properties properties) {
        this.properties = properties;
    }

    public <T> T inject(T objToInject) {
        try {
            Class<?> objClass = objToInject.getClass();
            Object instance = objClass.getDeclaredConstructor().newInstance();
            var fields = objClass.getDeclaredFields();

            for (var field : fields) {
                if (field.isAnnotationPresent(AutoInjectable.class)) {
                    field.setAccessible(true);
                    var fieldType = field.getType();
                    var fieldTypeName = fieldType.getName();

                    var typeToInjectName = properties.get(fieldTypeName).toString();
                    var fieldClass = Class.forName(typeToInjectName);
                    var fieldInstance = fieldClass.getDeclaredConstructor().newInstance();
                    field.set(instance, fieldInstance);
                }
            }
            return (T)instance;
        }
        catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }
}
