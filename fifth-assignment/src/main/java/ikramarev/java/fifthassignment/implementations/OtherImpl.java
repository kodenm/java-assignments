package ikramarev.java.fifthassignment.implementations;

import ikramarev.java.fifthassignment.interfaces.SomeInterface;

public class OtherImpl implements SomeInterface {
    public void doSomething() {
        System.out.print("B");
    }
}
