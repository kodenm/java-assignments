package ikramarev.java.fifthassignment.implementations;

import ikramarev.java.fifthassignment.interfaces.SomeInterface;

public class SomeImpl implements SomeInterface {
    public void doSomething() {
        System.out.print("A");
    }
}
