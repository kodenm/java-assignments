package ikramarev.java.fifthassignment;

import ikramarev.java.fifthassignment.annotations.AutoInjectable;
import ikramarev.java.fifthassignment.interfaces.SomeInterface;
import ikramarev.java.fifthassignment.interfaces.SomeOtherInterface;

public class SomeBean {
    @AutoInjectable
    private SomeInterface field1;

    @AutoInjectable
    private SomeOtherInterface field2;

    public void foo() {
        field1.doSomething();
        field2.doSomeOther();
    }
}
