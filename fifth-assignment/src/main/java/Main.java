import ikramarev.java.fifthassignment.Injector;
import ikramarev.java.fifthassignment.SomeBean;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        var injector = new Injector();
        var bean = injector.inject(new SomeBean());
        bean.foo();
    }
}
