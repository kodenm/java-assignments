package ru.vsu.ikramarev.java.firstassignment;

import java.util.Arrays;
import java.util.Objects;

/**
 * Two-dimensional array wrapper for matrix
 */
public class Matrix {
    /**
     * wrapped array
     */
    private final double[][] array;
    /**
     * number of columns in a matrix
     * can't be negative
     */
    private final int columns;
    /**
     * number of rows in a matrix
     * can't be negative
     */
    private final int rows;

    /**
     * creates a zeroed matrix from passed dimensions
     * @param columns number of columns
     * @param rows number of rows
     * @throws NegativeArraySizeException when matrix' dimensions are negative
     */
    public Matrix(int columns, int rows) throws NegativeArraySizeException {
        if (columns <= 0 || rows <= 0) {
            throw new NegativeArraySizeException("Matrix dimensions must be positive numbers");
        }

        this.columns = columns;
        this.rows = rows;
        array = new double[rows][columns];
    }

    /**
     * creates a matrix from an existing two-dimensional array
     * @param array source two-dimensional array
     * @throws ArrayStoreException when a two-dimensional passed array is null
     */
    public Matrix(double[][] array) throws ArrayStoreException {
        if (array == null) {
            throw new ArrayStoreException("This array can't be used as a matrix");
        }

        this.array = array;
        this.rows = array.length;
        this.columns = array[0].length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return columns == matrix.columns && rows == matrix.rows && Arrays.deepEquals(array, matrix.array);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(columns, rows);
        result = 31 * result + Arrays.hashCode(array);
        return result;
    }

    /**
     * gets matrix' row
     * @param rowIndex positive index of a row in a array
     * @return array of numbers
     * @throws ArrayIndexOutOfBoundsException when a passed index is out of matrix' sizes
     */
    public double[] getRow(int rowIndex) throws ArrayIndexOutOfBoundsException {
        if (rowIndex <= 0 || rowIndex > rows) {
            throw new ArrayIndexOutOfBoundsException("Selected row index is out of array's size bounds");
        }

        return array[rowIndex - 1];
    }

    /**
     * gets matrix' column
     * @param columnIndex positive index of a column in a array
     * @return array of numbers
     * @throws ArrayIndexOutOfBoundsException when a passed index is out of matrix' sizes
     */
    public double[] getColumn(int columnIndex) throws ArrayIndexOutOfBoundsException {
        if (columnIndex <= 0 || columnIndex > columns) {
            throw new ArrayIndexOutOfBoundsException("Selected column index is out of array's size bounds");
        }

        var resultColumn = new double[rows];
        for (int i = 0; i < rows; i++) {
            resultColumn[i] = array[i][columnIndex - 1];
        }
        return resultColumn;
    }

    /**
     * @return number of columns in a matrix
     */
    public int getColumns() {
        return columns;
    }

    /**
     * @return number of rows in a matrix
     */
    public int getRows() {
        return rows;
    }

    /**
     * @return if a matrix is square
     */
    public boolean isSquare() {
        return columns == rows;
    }

    /**
     * transposes a matrix
     * @return newly created transposed matrix
     */
    public Matrix transpose() {
        Matrix result = new Matrix(columns, rows);

        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < columns; ++col) {
                result.array[col][row] = array[row][col];
            }
        }

        return result;
    }

    /**
     * sub matrix from a matrix
     * @param exclude_row row index to exclude. must be positive
     * @param exclude_col column index to exclude must be positive
     * @return newly created matrix with an excluded row and columns
     */
    public Matrix subMatrix(int exclude_row, int exclude_col) {
        Matrix result = new Matrix(rows - 1, columns - 1);

        for (int row = 0, p = 0; row < rows; ++row) {
            if (row != exclude_row - 1) {
                for (int col = 0, q = 0; col < columns; ++col) {
                    if (col != exclude_col - 1) {
                        result.array[p][q] = array[row][col];
                        ++q;
                    }
                }
                ++p;
            }
        }
        return result;
    }

    /**
     * determinant of a matrix
     * @return number which is a matrix' determinant
     */
    public double determinant() {
        if (rows != columns) {
            return Double.NaN;
        }
        else {
            return this._determinant();
        }
    }

    /**
     * private member for private use
     * @return number which is a matrix' determinant
     */
    private double _determinant() {
        if (columns == 1) {
            return array[0][0];
        }
        else if (columns == 2) {
            return (array[0][0] * array[1][1] - array[0][1] * array[1][0]);
        }
        else {
            double result = 0.0;

            for (int col = 0; col < columns; ++col) {
                Matrix sub = subMatrix(1, col + 1);

                result += (Math.pow(-1, 1 + col + 1) *
                        array[0][col] * sub._determinant());
            }

            return result;
        }
    }

    /**
     * @return inversed matrix
     * @throws ArithmeticException when a matrix is square or its determinant is zero
     */
    public Matrix inverse() throws ArithmeticException {
        double determinant = determinant();

        if (rows != columns || determinant == 0.0) {
            throw new ArithmeticException("This matrix can't be inversed");
        }
        else {
            Matrix result = new Matrix(rows, columns);

            for (int row = 0; row < rows; ++row) {
                for (int col = 0; col < columns; ++col) {
                    Matrix sub = subMatrix(row + 1, col + 1);
                    result.array[col][row] = (1.0 / determinant * Math.pow(-1, row + col) * sub.determinant());
                }
            }

            return result;
        }
    }
}
