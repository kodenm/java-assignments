package ru.vsu.ikramarev.java.firstassignment;

/**
 * Contains functions to work with multiple matrices
 */
public class MatrixOperations {
    /**
     * adds a number to a matrix
     * @param matrix matrix to be added to
     * @param value value to be added
     * @return matrix with an added value
     * @throws NullPointerException when a passed matrix is null
     */
    public static Matrix add(Matrix matrix, double value) throws NullPointerException {
        if (matrix == null) {
            throw new NullPointerException("Matrix can't be null");
        }
        var resultMatrix = new double[matrix.getRows()][matrix.getColumns()];
        for (int i = 0; i < matrix.getRows(); i++) {
            var tempRow = matrix.getRow(i + 1);
            for (int j = 0; j < matrix.getColumns(); j++) {
                resultMatrix[i][j] = tempRow[j] + value;
            }
        }

        return new Matrix(resultMatrix);
    }

    /**
     * multiplies a matrix by a value
     * @param matrix matrix to be multiplied
     * @param value value to multiply by
     * @return matrix multiplied by a value
     */
    public static Matrix multiply(Matrix matrix, double value) {
        if (matrix == null) {
            throw new NullPointerException("Matrix can't be null");
        }

        var resultMatrix = new double[matrix.getRows()][matrix.getColumns()];
        for (int i = 0; i < matrix.getRows(); i++) {
            var matrixRow = matrix.getRow(i + 1);
            for (int j = 0; j < matrix.getColumns(); j++) {
                resultMatrix[i][j] = matrixRow[j] * value;
            }
        }

        return new Matrix(resultMatrix);
    }

    /**
     * multiplies two matrices
     * @param firstMatrix first n*m matrix
     * @param secondMatrix second m*k matrix
     * @return multiplied n*k matrix
     * @throws ArithmeticException when first matrix' columns are not equal to second matrix' rows
     */
    public static Matrix multiply(Matrix firstMatrix, Matrix secondMatrix) throws ArithmeticException {
        if (firstMatrix.getColumns() != secondMatrix.getRows()) {
            throw new ArithmeticException("Number of columns of the first matrix must be equal to the number of rows of the second array");
        }

        var resultMatrix = new double[firstMatrix.getRows()][secondMatrix.getColumns()];
        var n = firstMatrix.getRows();
        var k = secondMatrix.getColumns();
        var m = firstMatrix.getColumns();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                for (int l = 0; l < m; l++) {
                    resultMatrix[i][j] += firstMatrix.getRow(i + 1)[l] * secondMatrix.getRow(l + 1)[j];
                }
            }
        }

        return new Matrix(resultMatrix);
    }

    /**
     * adds two matrices
     * @param firstMatrix first n*m matrix
     * @param secondMatrix second n*m matrix
     * @return n*m matrix from two added matrix
     * @throws ArithmeticException when two matrices' dimensions are not equal
     * @throws NullPointerException when matrices are null
     */
    public static Matrix add(Matrix firstMatrix, Matrix secondMatrix) throws ArithmeticException, NullPointerException {
        if (firstMatrix == null || secondMatrix == null) {
            throw new NullPointerException("Matrices can't be null");
        }
        if (firstMatrix.getColumns() != secondMatrix.getColumns() || firstMatrix.getRows() != secondMatrix.getRows()) {
            throw new ArithmeticException("Matrix with different dimensions can't be added");
        }

        var resultMatrix = new double[firstMatrix.getRows()][firstMatrix.getColumns()];
        for (int i = 0; i < firstMatrix.getRows(); i++) {
            var firstMatrixRow = firstMatrix.getRow(i + 1);
            var secondMatrixRow = secondMatrix.getRow(i + 1);
            for (int j = 0; j < firstMatrix.getColumns(); j++) {
                resultMatrix[i][j] = firstMatrixRow[j] + secondMatrixRow[j];
            }
        }

        return new Matrix(resultMatrix);
    }

    /**
     * subs second matrix from first
     * @param firstMatrix first n*m matrix
     * @param secondMatrix second n*m matrix
     * @return n*m matrix from subbed matrices
     * @throws ArithmeticException when two matrices' dimensions are not equal
     */
    public static Matrix sub(Matrix firstMatrix, Matrix secondMatrix) throws ArithmeticException {
        if (firstMatrix.getColumns() != secondMatrix.getColumns() || firstMatrix.getRows() != secondMatrix.getRows()) {
            throw new ArithmeticException("Matrix with different dimensions can't be added");
        }

        return add(firstMatrix, multiply(secondMatrix, -1));
    }
}
