package ru.vsu.ikramarev.java.firstassignment.tests;

import org.junit.jupiter.api.Test;
import ru.vsu.ikramarev.java.firstassignment.Matrix;
import ru.vsu.ikramarev.java.firstassignment.MatrixOperations;

import java.beans.Expression;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.*;

class MatrixOperationsTest {

    @Test
    void add_returnsMatrixAddedWithNumber() {
        var sourceMatrix = new Matrix(2, 2);
        var value = 1;

        double[][] expectedMatrixArray = { {1, 1}, {1, 1} };
        var expectedMatrix = new Matrix(expectedMatrixArray);

        var resultMatrix = MatrixOperations.add(sourceMatrix, value);
        assertEquals(resultMatrix, expectedMatrix);
    }

    @Test
    void add_throwsNullPointerExceptionWhenMatrixIsNull() {
        Matrix matrix = null;
        int num = 1;

        assertThrows(NullPointerException.class, () -> MatrixOperations.add(matrix, num));
    }

    @Test
    void add_returnsMatrixAddedWithAnotherMatrix() {
        var firstSourceMatrix = MatrixOperations.add(new Matrix(2, 2), 1);
        var secondSourceMatrix = MatrixOperations.add(new Matrix(2, 2), 2);

        double[][] expectedMatrixArray = { {3, 3}, {3, 3} };
        var expectedMatrix = new Matrix(expectedMatrixArray);

        var resultMatrix = MatrixOperations.add(firstSourceMatrix, secondSourceMatrix);
        assertEquals(resultMatrix, expectedMatrix);
    }

    @Test
    void add_throwsArithmeticExceptionWhenTwoMatricesSizesAreDifferent() {
        var firstSourceMatrix = MatrixOperations.add(new Matrix(2, 2), 1);
        var secondSourceMatrix = MatrixOperations.add(new Matrix(3, 2), 2);

        assertThrows(ArithmeticException.class, () -> MatrixOperations.add(firstSourceMatrix, secondSourceMatrix));
    }

    @Test
    void add_throwsNullPointerWhenAnyOfMatricesAreNull() {
        var firstSourceMatrix = MatrixOperations.add(new Matrix(2, 2), 1);
        var secondSourceMatrix = (Matrix)null;

        assertThrows(NullPointerException.class, () -> MatrixOperations.add(firstSourceMatrix, secondSourceMatrix));
    }

    @Test
    void multiply_throwsNullPointerExceptionWhenMatrixIsNull() {
        Matrix matrix = null;
        int num = 1;

        assertThrows(NullPointerException.class, () -> MatrixOperations.multiply(matrix, num));
    }

    @Test
    void multiply_returnsMatrixAddedWithNumber() {
        double[][] sourceMatrixArray = { {1, 1}, {1, 1} };
        var sourceMatrix = new Matrix(sourceMatrixArray);
        var value = 2;

        double[][] expectedMatrixArray = { {2, 2}, {2, 2} };
        var expectedMatrix = new Matrix(expectedMatrixArray);

        var resultMatrix = MatrixOperations.multiply(sourceMatrix, value);
        assertEquals(resultMatrix, expectedMatrix);
    }

    @Test
    void multiply_throwsNullPointerExceptionWhenOneOfMatricesAreNull() {
        var firstSourceMatrix = MatrixOperations.add(new Matrix(2, 2), 1);
        var secondSourceMatrix = (Matrix)null;

        assertThrows(NullPointerException.class, () -> MatrixOperations.multiply(firstSourceMatrix, secondSourceMatrix));
    }

    @Test
    void multiply_throwsArithmeticExceptionWhenMatricesHaveNoCommonSize() {
        var firstSourceMatrix = MatrixOperations.add(new Matrix(2, 2), 1);
        var secondSourceMatrix = MatrixOperations.add(new Matrix(3, 3), 1);

        assertThrows(ArithmeticException.class, () -> MatrixOperations.multiply(firstSourceMatrix, secondSourceMatrix));
    }

    @Test
    void multiply_returnMultipliedMatrixOfTwos() {
        double[][] firstSourceMatrixArray =
                {   { 1, 1, 1 },
                    { 2, 2, 2 },
                    { 3, 3, 3 },
                    { 4, 4, 4 } };
        var firstSourceMatrix = new Matrix(firstSourceMatrixArray);
        double[][] secondSourceMatrixArray =
                {   { 1, 1, 1, 1 },
                    { 2, 2, 2, 2 },
                    { 3, 3, 3, 3 } };
        var secondSourceMatrix = new Matrix(secondSourceMatrixArray);

        double[][] expectedMultipliedMatrixArray =
                {       { 6, 6, 6, 6 },
                        { 12, 12, 12, 12 },
                        { 18, 18, 18, 18 },
                        { 24, 24, 24, 24 }
                };
        var expectedMatrix = new Matrix(expectedMultipliedMatrixArray);
        var resultMatrix = MatrixOperations.multiply(firstSourceMatrix, secondSourceMatrix);

        assertEquals(expectedMatrix, resultMatrix);
    }

    @Test
    void sub() {
    }
}