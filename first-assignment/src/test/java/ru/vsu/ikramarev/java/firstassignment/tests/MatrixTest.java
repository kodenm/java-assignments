package ru.vsu.ikramarev.java.firstassignment.tests;

import org.junit.jupiter.api.Test;
import ru.vsu.ikramarev.java.firstassignment.Matrix;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {
    @Test
    /**
     * Asserts if a matrix creates on a valid passed array
     */
    void createsOnValidArray(){
        // arrange
        double[][] matrixArray = new double[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                matrixArray[i][j] = i + j / 10;
            }
        }

        // act
        // assert
        assertAll(() -> new Matrix(matrixArray));
    }

    @Test
    /**
     * Asserts if a matrix creates on a valid sizes
     */
    void createsOnValidSizes(){
        // arrange
        int rows = 1;
        int columns = 2;

        // act
        // assert
        assertAll(() -> new Matrix(rows, columns));
    }

    @Test
    /**
     * Asserts if an ArrayStoreException throws when null array is passed to a constructor of a matrix
     */
    void failsToCreateOnNullReferenceArray(){
        // arrange
        double[][] matrixArray = null;

        // act
        // assert
        assertThrows(ArrayStoreException.class, () -> new Matrix(matrixArray));
    }

    @Test
    /**
     * Asserts if an NegativeArraySizeException throws when negative sizes are passed to a constructor of a matrix
     */
    void failsToCreateOnInvalidSizes() {
        // arrange
        int rows = -1;
        int columns = 5;

        // act
        // assert
        assertThrows(NegativeArraySizeException.class, () -> new Matrix(rows, columns));
    }

    @Test
    void getsValidRow() {
        // arrange
        double[][] matrixArray = new double[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                matrixArray[i][j] = i + j / 10;
            }
        }
        var expectedRow = matrixArray[0];

        // act
        var matrix = new Matrix(matrixArray);
        // assert
        assertTrue(Arrays.equals(expectedRow, matrix.getRow(1)));
    }

    @Test
    void throwsArrayIndexOutOfBoundsExceptionOnBadRowIndex() {
        // arrange
        double[][] matrixArray = new double[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                matrixArray[i][j] = i + j / 10;
            }
        }

        // act
        var matrix = new Matrix(matrixArray);
        // assert
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> matrix.getRow(-1));
    }

    @Test
    void getsValidColumn() {
        // arrange
        double[][] matrixArray = new double[2][2];
        var expectedColumn = new double[2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                matrixArray[i][j] = i + j / 10;
            }
            expectedColumn[i] = matrixArray[i][0];
        }

        // act
        var matrix = new Matrix(matrixArray);

        // assert
        assertTrue(Arrays.equals(expectedColumn, matrix.getColumn(1)));
    }

    @Test
    void throwsArrayIndexOutOfBoundsExceptionOnBadColumnIndex() {
        // arrange
        double[][] matrixArray = new double[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                matrixArray[i][j] = i + j / 10;
            }
        }

        // act
        var matrix = new Matrix(matrixArray);
        // assert
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> matrix.getColumn(-1));
    }

    @Test
    void returnsActualNumberOfColumns() {
        int rows = 1;
        int columns = 1;
        var matrix = new Matrix(rows, columns);

        assertEquals(columns, matrix.getColumns());
    }

    @Test
    void returnsActualNumberOfRows() {
        int rows = 1;
        int columns = 1;
        var matrix = new Matrix(rows, columns);

        assertEquals(columns, matrix.getRows());
    }

    @Test
    void returnsTrueIfActuallySquare() {
        int rows = 1;
        int columns = 1;
        var matrix = new Matrix(rows, columns);

        assertTrue(matrix.isSquare());
    }

    @Test
    void returnsFalseIfNotSquare() {
        int rows = 1;
        int columns = 2;
        var matrix = new Matrix(rows, columns);

        assertFalse(matrix.isSquare());
    }

    @Test
    void returnsValidTransposedMatrix() {
        double[][] matrixArray = { {1, 2}, {3, 4} };
        var matrix = new Matrix(matrixArray);

        double[][] expectedTransposedMatrixArray = { { 1, 3 }, { 2, 4 } };
        var expectedTransposedMatrix = new Matrix(expectedTransposedMatrixArray);

        var transposedMatrix = matrix.transpose();
        assertEquals(expectedTransposedMatrix, transposedMatrix);
    }

    @Test
    void returnsValidSubMatrix() {
        double[][] matrixArray = { {1, 2}, {3, 4} };
        var matrix = new Matrix(matrixArray);

        double[][] expectedSubMatrixArray = { { 4 } };
        var expectedTransposedMatrix = new Matrix(expectedSubMatrixArray);

        var subMatrix = matrix.subMatrix(1, 1);
        assertEquals(expectedTransposedMatrix, subMatrix);
    }

    @Test
    void returnsValidDeterminantOf3x3Matrix() {
        double[][] matrixArray = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} };
        var matrix = new Matrix(matrixArray);

        double expectedDeterminant = 0;
        double resultDeterminant = matrix.determinant();

        assertEquals(expectedDeterminant, resultDeterminant);
    }

    @Test
    void returnsValidDeterminantOf2x2Matrix() {
        double[][] matrixArray = { {1, 2}, {3, 4} };
        var matrix = new Matrix(matrixArray);

        double expectedDeterminant = -2;
        double resultDeterminant = matrix.determinant();

        assertEquals(expectedDeterminant, resultDeterminant);
    }

    @Test
    void returnsNanIfSizesAreNotEqual() {
        double[][] matrixArray = { {1, 2}, {3, 4}, {5, 6} };
        var matrix = new Matrix(matrixArray);

        double expectedDeterminant = Double.NaN;
        double resultDeterminant = matrix.determinant();
        assertEquals(expectedDeterminant, resultDeterminant);
    }

    @Test
    void throwsArithmeticExceptionWhenDeterminantIsZero() {
        double[][] matrixArray = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} };
        var matrix = new Matrix(matrixArray);

        assertThrows(ArithmeticException.class, () -> matrix.inverse());
    }

    @Test
    void returnsInversedMatrix() {
        double[][] matrixArray = { {1, 2, 3}, {4, 10, 6}, {7, 8, 9} };
        var matrix = new Matrix(matrixArray);

        double[][] expectedInversedMatrixArray = { { -7.0/10, -1.0/10, 3.0/10 }, {-1.0/10, 1.0/5, -1.0/10}, {19.0/30, -1.0/10, -1.0/30}};
        var expectedInversedMatrix = new Matrix(expectedInversedMatrixArray);
        var resultInversedMatrix = matrix.inverse();
        assertEquals(expectedInversedMatrix, resultInversedMatrix);
    }
}